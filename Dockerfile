FROM openjdk:21-jdk

LABEL MAINTAINER="Yunni"
LABEL Description="Dockerize Springboot with base image openjdk 11"

WORKDIR /app
RUN adduser spring
ARG JAR_FILE=target/*-SNAPSHOT.jar
ARG appname=middleware-transfer-1.0.0-SNAPSHOT.jar
COPY ${JAR_FILE} middleware-transfer-1.0.0-SNAPSHOT.jar
RUN chown spring: middleware-transfer-1.0.0-SNAPSHOT.jar
USER spring
EXPOSE 8080

ENTRYPOINT ["java","-jar","/app/middleware-transfer-1.0.0-SNAPSHOT.jar"]