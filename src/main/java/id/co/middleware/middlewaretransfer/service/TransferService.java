package id.co.middleware.middlewaretransfer.service;

import id.co.middleware.middlewaretransfer.dto.*;
import id.co.middleware.middlewaretransfer.exception.MaximumTransactionLimitException;
import id.co.middleware.middlewaretransfer.exception.MinimumTransactionLimitException;
import id.co.middleware.middlewaretransfer.exception.TransactionFailedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class TransferService {

    @Autowired
    private SwitcingService swicingService;

    @Autowired
    private CoreService coreService;

    @Autowired
    private LimitManagementService limitManagementService;

    public TransferOfUsInquiryResponse inquiry(TransferOfUsInquiryRequest request){

        GlobalLimit globalLimit = null;
        var globalLimitListResponse = limitManagementService.getGlobalLimit(GetGlobalLimitRequest.builder().id(426L).build());
        globalLimit = globalLimitListResponse.getGlobalLimitList().getFirst();
        // Validate Minimum Amount
        if (request.getAmount().compareTo(globalLimit.getMinAmountTransaction()) < 0) {
            throw new MinimumTransactionLimitException("Transaction amount is less than minimum transaction amount");
        }

        // Validate Maximum Amount
        if (request.getAmount().compareTo(globalLimit.getMaxAmountTransaction()) > 0) {
            throw new MaximumTransactionLimitException("Transaction amount is more than maximum transaction amount");
        }

        return swicingService.inquiry(request);

    }

    public TransferOfUsConfirmResponse execute(TransferOfUsConfirmRequest request){

        var executeCore =  coreService.execute(request);

        if(!executeCore.getResponseCode().equals("00")){
            throw new TransactionFailedException(executeCore.getMessage());
        }

        return swicingService.execute(request);
    }

}
