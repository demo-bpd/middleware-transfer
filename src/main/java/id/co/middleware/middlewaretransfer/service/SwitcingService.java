package id.co.middleware.middlewaretransfer.service;

import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmResponse;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class SwitcingService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${inquirytransferswitching}")
    private String inquirytransferswitching;
    @Value("${executetransferswitching}")
    private String executetransferswitching;

    public TransferOfUsInquiryResponse inquiry(TransferOfUsInquiryRequest request){
        log.info("start request inquiry switching");
        TransferOfUsInquiryResponse response = restTemplate.postForEntity(inquirytransferswitching, request, TransferOfUsInquiryResponse.class).getBody();
        log.info("response inquiry switching {}",response);

        return response;
    }

    public TransferOfUsConfirmResponse execute(TransferOfUsConfirmRequest request){
        log.info("start request execute switching");
        TransferOfUsConfirmResponse response= restTemplate.postForEntity(executetransferswitching, request, TransferOfUsConfirmResponse.class).getBody();
        log.info("response execute switching {}",response);
        return response;
    }
}
