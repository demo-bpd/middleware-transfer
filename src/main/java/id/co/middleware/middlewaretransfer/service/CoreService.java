package id.co.middleware.middlewaretransfer.service;


import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CoreService {

    @Autowired
    private RestTemplate restTemplate;


    @Value("${executetransfercore}")
    private String executetransfercore;


    public TransferOfUsConfirmResponse execute(TransferOfUsConfirmRequest request){
        log.info("start request execute core");
        TransferOfUsConfirmResponse response= restTemplate.postForEntity(executetransfercore, request, TransferOfUsConfirmResponse.class).getBody();
        log.info("response execute core {}",response);

        return response;
    }
}
