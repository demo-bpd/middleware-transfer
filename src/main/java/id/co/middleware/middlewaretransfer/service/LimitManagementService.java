package id.co.middleware.middlewaretransfer.service;

import id.co.middleware.middlewaretransfer.dto.GetGlobalLimitRequest;
import id.co.middleware.middlewaretransfer.dto.GlobalLimitListResponse;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class LimitManagementService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${globallimiturl}")
    private String globallimiturl;

    public GlobalLimitListResponse getGlobalLimit(GetGlobalLimitRequest request){
        log.info("start request get global limit");
        GlobalLimitListResponse response =
                restTemplate.postForEntity(globallimiturl, request, GlobalLimitListResponse.class).getBody();
        log.info("response get limit {}",response);
        return response;
    }
}
