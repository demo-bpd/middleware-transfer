package id.co.middleware.middlewaretransfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiddlewareTransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiddlewareTransferApplication.class, args);
	}

}
