package id.co.middleware.middlewaretransfer.controller;

import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsConfirmResponse;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryRequest;
import id.co.middleware.middlewaretransfer.dto.TransferOfUsInquiryResponse;
import id.co.middleware.middlewaretransfer.service.TransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class TransferController {

    @Autowired
    private TransferService transferService;


    @PostMapping(value = "/inquiryTransferOffUss")
    public TransferOfUsInquiryResponse inquiryTransferOffUss(@RequestBody TransferOfUsInquiryRequest request){
        log.info("start inquiryTransferOffUss");
        return transferService.inquiry(request);
    }


    @PostMapping(value = "/executeTransferOffUss")
    public TransferOfUsConfirmResponse executeTransferOffUss(@RequestBody TransferOfUsConfirmRequest request){
        log.info("start executeTransferOffUss");
        return transferService.execute(request);
    }

}
