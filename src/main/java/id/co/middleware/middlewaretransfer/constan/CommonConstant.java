package id.co.middleware.middlewaretransfer.constan;

public class CommonConstant {

    private CommonConstant() {}

    public static final int REDIS_NO_TTL = 0;
    public static final String TRACE_ID_FIELD = "traceId";

    public static class CacheName {
        private CacheName() {}

        public static final String ALL = "ALL";
        public static final String MENU = "MENU";
        public static final String CONFIG_PARAM = "CONFIG";
        public static final String LOCATION = "LOCATION";
        public static final String PUBLISHED_PROMO = "PUBLISH";
        public static final String BRANCH = "BRANCH";
        public static final String OTP_REQUEST = "OTP_REQUEST";
        public static final String PRODUCT_LIST = "PRODUCT_LIST";
        public static final String SALESMAN = "SALESMAN";
        public static final String PRODUCT = "PRODUCT";


    }

    public static class CacheKey {
        private CacheKey() {}

        public static final String LIST = "LIST";
        public static final String MAP = "MAP";
        public static final String SAP_HYBRIS_CLIENT_ID = "sap-hybris.client-id";
        public static final String SAP_HYBRIS_CLIENT_SECRET = "sap-hybris.client-secret";
        public static final String SAP_HYBRIS_GRANT_TYPE = "sap-hybris.grant-type";
        public static final String BRANCH = "BRANCH";

        public static final String PROVINCE = "PROVINCE";
        public static final String CITIES = "CITIES";
        public static final String CATEGORY = "CATEGORY";
        public static final String CODE = "CODE";
        public static final String CONTENT_CATEGORIES = "CONTENT_CATEGORIES";
        public static final String PRODUCT_ = "PRODUCT_";

        public static final String MAX_OTP_ATTEMPTS = "max_otp_attempts";

        public static final String OTP_ATTEMP= "OTP_ATTEMP";
        public static final String MULESOFT_CLIENT_ID = "mulesoft.client-id";
        public static final String MULESOFT_CLIENT_SECRET = "mulesoft.client-secret";
        public static final String MULESOFT_GRANT_TYPE = "mulesoft.grant-type";
        public static final String MULESOFT_SCOPE= "mulesoft.scope";

        public static final String KEYCLOAK_CLIENT_ID = "keycloak.client-id";
        public static final String KEYCLOAK_CLIENT_SECRET = "keycloak.client-secret";


    }

    public static class CategoryType {
        private CategoryType() {}
        public static final String NEWS= "news";
        public static final String PROMO = "promo";
        public static final String PRODUCT_KNOWLEDGE = "productknowledge";
    }

}
