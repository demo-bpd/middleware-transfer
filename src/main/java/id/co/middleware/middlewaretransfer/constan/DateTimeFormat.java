package id.co.middleware.middlewaretransfer.constan;

public class DateTimeFormat {

    private DateTimeFormat() {}

    public static final String RFC_3339 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static final String DEFAULT_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_TIME_FORMAT_1 = "dd MMM yyy HH:mm";
    public static final String DATE_TIME_FORMAT_2 = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT_3 = "dd-MM-yyyy";
    public static final String DATE_TIME_FORMAT_4 = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT_5 = "dd MMM yyyy";
    public static final String DATE_TIME_FORMAT_6 = "dd-MMM-yy";
    public static final String DATE_TIME_FORMAT_7 = "dd-MMM-yyyy";
    public static final String DATE_TIME_FORMAT_8 = "dd MMMM yyyy";

}
