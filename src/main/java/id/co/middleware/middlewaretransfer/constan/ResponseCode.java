package id.co.middleware.middlewaretransfer.constan;

public class ResponseCode {
    private ResponseCode() {
    }

    public static final String SUCCESS = "00";
    public static final String NOT_FOUND = "0033";
    public static final String FAILED = "99";
    public static final String EXCEED_DAILY_TRANSACTION_LIMIT = "0021";
    public static final String MINIMUM_TRANSACTION_LIMIT = "0040";
    public static final String MAXIMUM_TRANSACTION_LIMIT = "0041";
    public static final String PREFIX_ERROR = "0098";
    public static final String TRANSACTION_ALREADY = "0099";
    public static final String TRANSACTION_FAILED = "0100";
    public static final String EXCEED_LIMIT = "EXCDLMT";
}
