package id.co.middleware.middlewaretransfer.constan;

public class MessageContent {

    private MessageContent() {}

    public static final String SUCCESS = "SUCCESS";
    public static final String PAGE_NOT_EXIST = "Page does not exists";
    public static final String DATA_NOT_EXISTS = "Data does not exists";
    public static final String GENERAL_ERROR = "Internal Server Error, please contact administrator.";
    public static final String OTP_MAX_ATTEMPS = "You have reach max otp request";

}
