package id.co.middleware.middlewaretransfer.exception;

public class MaximumTransactionLimitException extends RuntimeException {

	private static final long serialVersionUID = -977386496085013686L;

	public MaximumTransactionLimitException(String message) {
        super(message);
    }

}
