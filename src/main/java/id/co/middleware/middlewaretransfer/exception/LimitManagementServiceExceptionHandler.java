package id.co.middleware.middlewaretransfer.exception;


import co.id.ist.digitalbanking.apicommon.constant.ErrorDetailConstant;
import co.id.ist.digitalbanking.apicommon.dto.ErrorDetail;
import id.co.middleware.middlewaretransfer.constan.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class LimitManagementServiceExceptionHandler  {

    private ResponseEntity<Object> buildResponseEntity(String code, String sourceSystem, String message) {

            var errorDetail = ErrorDetail.builder()
                    .errorCode(code)
                    .sourceSystem(sourceSystem)
                    .engMessage(message)
                    .idnMessage(message)
                    .message(message)
                    .activityRefCode(MDC.get(ErrorDetailConstant.TRACE_ID))
                    .build();
            return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> userNotFoundException(NotFoundException e) {
        log.info("Handling NotFoundException");
        return buildResponseEntity(ResponseCode.NOT_FOUND,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(MinimumTransactionLimitException.class)
    public ResponseEntity<Object> minimumTransactionException(MinimumTransactionLimitException e) {
        log.info("Handling MinimumTransactionLimitException");
        return buildResponseEntity(ResponseCode.MINIMUM_TRANSACTION_LIMIT,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(MaximumTransactionLimitException.class)
    public ResponseEntity<Object> maximumTransactionException(MaximumTransactionLimitException e) {
        log.info("Handling MaximumTransactionLimitException");
        return buildResponseEntity(ResponseCode.MAXIMUM_TRANSACTION_LIMIT,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(ExceedDailyTransactionLimitException.class)
    public ResponseEntity<Object> exceedDailyTransactionLimitException(ExceedDailyTransactionLimitException e) {
        log.info("Handling ExceedDailyTransactionLimitException");
        return buildResponseEntity(ResponseCode.EXCEED_DAILY_TRANSACTION_LIMIT,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(ExceedLimitException.class)
    public ResponseEntity<Object> exceedLimitException(ExceedLimitException e) {
        log.info("Handling ExceedLimitException");
        return buildResponseEntity(ResponseCode.EXCEED_LIMIT,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(TransactionAlreadyException.class)
    public ResponseEntity<Object> transactionAlreadyException(TransactionAlreadyException e) {
        log.info("Handling transactionAlreadyException");
        return buildResponseEntity(ResponseCode.TRANSACTION_ALREADY,
                "middleware", e.getMessage());
    }

    @ExceptionHandler(TransactionFailedException.class)
    public ResponseEntity<Object> transactionAlreadyException(TransactionFailedException e) {
        log.info("Handling ExceedLimitException");
        return buildResponseEntity(ResponseCode.TRANSACTION_ALREADY,
                "middleware", e.getMessage());
    }
}
