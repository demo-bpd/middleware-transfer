package id.co.middleware.middlewaretransfer.exception;

public class ExceedLimitException extends RuntimeException {

	private static final long serialVersionUID = 3890539464598841830L;

	public ExceedLimitException(String message) {
        super(message);
    }

}
