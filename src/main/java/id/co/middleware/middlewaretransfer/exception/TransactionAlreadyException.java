package id.co.middleware.middlewaretransfer.exception;

public class TransactionAlreadyException extends RuntimeException {

    private static final long serialVersionUID = 2856472444663512791L;

    public TransactionAlreadyException(String message) {
        super(message);
    }

}
