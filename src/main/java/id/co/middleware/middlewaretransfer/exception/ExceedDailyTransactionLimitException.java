package id.co.middleware.middlewaretransfer.exception;

public class ExceedDailyTransactionLimitException extends RuntimeException {

	private static final long serialVersionUID = -8536760029543384470L;

	public ExceedDailyTransactionLimitException(String message) {
        super(message);
    }

}
