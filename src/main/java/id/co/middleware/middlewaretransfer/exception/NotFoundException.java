package id.co.middleware.middlewaretransfer.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 2856472444663512791L;

    public NotFoundException(String message) {
        super(message);
    }

}
