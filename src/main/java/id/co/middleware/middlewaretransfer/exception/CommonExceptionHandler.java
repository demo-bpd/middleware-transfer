package id.co.middleware.middlewaretransfer.exception;


import id.co.middleware.middlewaretransfer.dto.ErrorResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@Slf4j
@RestControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleMissingServletRequestParameterException(MissingServletRequestParameterException exception,
                                                                                       HttpServletRequest httpServletRequest) {
        return this.generateResponse(exception.getMessage(), httpServletRequest, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = {CommonException.class})
    public ResponseEntity<ErrorResponse> commonExceptionHandler(CommonException exception, HttpServletRequest httpServletRequest) {
        log.debug("CommonException, ", exception);
        return this.generateResponse(exception.getMessage(), httpServletRequest, exception.getStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleGenericException(Exception exception, HttpServletRequest httpServletRequest) {
        log.debug("Unknown Error, ", exception);
        return this.generateResponse("Internal Server Error, please contact administrator.", httpServletRequest, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorResponse> generateResponse(String message, HttpServletRequest httpServletRequest, HttpStatus httpStatus) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(httpStatus.value());
        errorResponse.setError(message);
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setPath(httpServletRequest.getRequestURI());

        return new ResponseEntity<>(errorResponse, httpStatus);
    }

}
