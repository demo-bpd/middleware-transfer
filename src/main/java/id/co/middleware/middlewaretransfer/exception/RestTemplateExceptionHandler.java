package id.co.middleware.middlewaretransfer.exception;

import id.co.middleware.middlewaretransfer.constan.MessageContent;
import id.co.middleware.middlewaretransfer.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpMessageConverterExtractor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
public class RestTemplateExceptionHandler<T> extends DefaultResponseErrorHandler {

    private final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
    private final HttpMessageConverterExtractor<T> jacksonMessageConverter;

    public RestTemplateExceptionHandler(Class<T> responseType) {
        this.messageConverters.add(new MappingJackson2HttpMessageConverter());
        this.jacksonMessageConverter = new HttpMessageConverterExtractor<>(responseType, this.messageConverters);
    }

    protected void handleError(ClientHttpResponse response, HttpStatus statusCode) throws IOException {
        try {
            ResponseEntity<T> responseEntity = new ResponseEntity<>(this.jacksonMessageConverter.extractData(response), response.getHeaders(), response.getStatusCode());
            ErrorResponse errorResponse = (ErrorResponse) Objects.requireNonNull(responseEntity.getBody());
            throw new CommonException(errorResponse.getError(), statusCode);
        } catch (CommonException e) {
            log.debug("Exception when handling rest client error. Return response server error. ", e);
            throw e;
        } catch (Exception e) {
            log.error("Exception when handling rest client error. Return response internal server error. ", e);
            throw new CommonException(MessageContent.GENERAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
