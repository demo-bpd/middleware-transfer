package id.co.middleware.middlewaretransfer.exception;

public class MinimumTransactionLimitException extends RuntimeException {

	private static final long serialVersionUID = -9211201939805829139L;

	public MinimumTransactionLimitException(String message) {
        super(message);
    }

}
