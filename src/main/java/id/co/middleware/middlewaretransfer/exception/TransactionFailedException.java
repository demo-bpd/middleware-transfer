package id.co.middleware.middlewaretransfer.exception;

public class TransactionFailedException extends RuntimeException {

    private static final long serialVersionUID = 2856472444663512791L;

    public TransactionFailedException(String message) {
        super(message);
    }

}
