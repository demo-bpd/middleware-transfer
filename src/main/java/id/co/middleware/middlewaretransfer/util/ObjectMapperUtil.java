package id.co.middleware.middlewaretransfer.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import id.co.middleware.middlewaretransfer.constan.DateTimeFormat;

import java.text.SimpleDateFormat;

public class ObjectMapperUtil {

    private ObjectMapperUtil() {}

    public static final ObjectMapper MAPPER = getMapper();

    public static <T> T convert(Object source, Class<T> targetClass) {
        return MAPPER.convertValue(source, targetClass);
    }

    public static <T> T convert(Object source, TypeReference<T> toValueTypeRef) {
        return MAPPER.convertValue(source, toValueTypeRef);
    }

    private static ObjectMapper getMapper() {
        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .setDateFormat(new SimpleDateFormat(DateTimeFormat.DEFAULT_DATE_TIME_FORMAT));
    }

}
