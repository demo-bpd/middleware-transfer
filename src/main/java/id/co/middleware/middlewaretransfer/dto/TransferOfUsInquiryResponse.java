package id.co.middleware.middlewaretransfer.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferOfUsInquiryResponse  extends BaseTransferRs implements Serializable {

    private static final long serialVersionUID = -1027158150120900957L;
    private TransferData data;
}
