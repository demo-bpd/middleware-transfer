package id.co.middleware.middlewaretransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GlobalLimit implements Serializable {

    private Long id;

    private Date createdDate;
    private String createdBy;

    private Date modifiedDate;

    private String modifiedBy;

    private String currencyCode;

    private BigDecimal maxAmountTransaction;

    private BigDecimal minAmountTransaction;

    private BigDecimal maxAmountTransactionOfDay;

    private Integer maxTransaction;

    private BigInteger isDeleted;
}
