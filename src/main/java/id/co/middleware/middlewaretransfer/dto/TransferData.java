package id.co.middleware.middlewaretransfer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferData {
    private String destCustomerName;
    private String custReffNo;
    private String issuerCustomerName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fromAccountNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String toAccountNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String retrievalReferenceNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String localDateTime;
    private String note;
}
