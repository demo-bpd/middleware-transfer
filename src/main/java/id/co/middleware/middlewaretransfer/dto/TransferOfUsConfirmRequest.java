package id.co.middleware.middlewaretransfer.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferOfUsConfirmRequest implements Serializable {

    private static final long serialVersionUID = -7873232593745539378L;

    private BigDecimal adminFee;
    private String destCustomerName;
    private String note;
    private String receivingInstitutionId;
    private String primaryAccountNumber;
    private String settlementDate;
    private String transactionType;


    private String currencyCode;
    private String transactionId;
    private BigDecimal amount;
    private String fromAccountNumber;
    private String issuerCode;
    private String localDateTime;
    private String retrievalReferenceNumber;
    private Long systemTraceAuditNumber;
    private String toAccountNumber;

}
