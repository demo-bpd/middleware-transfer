package id.co.middleware.middlewaretransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GlobalLimitListResponse implements Serializable {
    private static final long serialVersionUID = 4378969525217080138L;
    private List<GlobalLimit> globalLimitList;
}
