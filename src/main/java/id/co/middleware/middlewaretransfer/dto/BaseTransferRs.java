package id.co.middleware.middlewaretransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseTransferRs implements Serializable {
    private static final long serialVersionUID = 7766734308379075172L;

    private String status;
    private String message;
    private String responseCode;
}
