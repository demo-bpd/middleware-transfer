package id.co.middleware.middlewaretransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetGlobalLimitRequest implements Serializable {

    private static final long serialVersionUID = 4162590485522178853L;
    private Long id;
}
