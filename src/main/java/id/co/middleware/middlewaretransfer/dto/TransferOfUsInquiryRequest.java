package id.co.middleware.middlewaretransfer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferOfUsInquiryRequest implements Serializable {

    private static final long serialVersionUID = 494240990195987686L;

    private String receivingInstitutionId;    //bank code receiving
    private String primaryAccountNumber;
    private String settlementDate;
//    String transactionType;
    private BigDecimal adminFee;
//    String phoneNumber;
    private String note;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String currencyCode;
    private String transactionId;
//    String acquirerCode;
//    String acquiringInstitutionId;
    private BigDecimal amount;
//    String beneficiaryCode;
//    String caIdentificationCode;
//    String caTerminalId;
//    String customerReffNo;
    private String fromAccountNumber;
    private String issuerCode;
    private String issuerCustomerName;
    private String localDateTime;
    private String channelCode;
    private String retrievalReferenceNumber;
    private Long systemTraceAuditNumber;
//    String terminalCity;
//    String terminalCountry;
//    String terminalOwner;
//    String terminalState;
    private String toAccountNumber;
//    int residentType;
//    int benefitRecipients;


}
