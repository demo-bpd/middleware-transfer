package id.co.middleware.middlewaretransfer.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferOfUsConfirmResponse  extends BaseTransferRs implements Serializable{

    private static final long serialVersionUID = -715279175461624443L;

    private TransferData data;
}
