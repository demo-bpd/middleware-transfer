package id.co.middleware.middlewaretransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseTransferRq implements Serializable {

    private static final long serialVersionUID = 1292135207867394653L;

    Long accountType;
    String acquirerCode;
    String acquiringInstitutionId;
    BigDecimal amount;
    String beneficiaryCode;
    String caIdentificationCode;
    String caTerminalId;
    String currencyCode;
    String customerReffNo;
    String fromAccountNumber;
    String issuerCode;
    String issuerCustomerName;
    String localDateTime;
    String merchantType;
    String retrievalReferenceNumber;
    Long systemTraceAuditNumber;
    String terminalCity;
    String terminalCountry;
    String terminalOwner;
    String terminalState;
    String toAccountNumber;
}
